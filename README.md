# dotfiles - the @S1SYPHOS way

This repository houses the [dotfiles](https://dotfiles.github.io) used when setting up my [personal workstation](https://github.com/S1SYPHOS/workstation). It's powered by Anish Athalye's [`dotbot`](https://github.com/anishathalye/dotbot), which makes symlinking dotfiles a breeze.

Notable features:

- **OS**: [Fedora](https://getfedora.org)
- **WM**: [Sway](https://swaywm.org)
- **Shell**: [Bash](http://www.gnu.org/software/bash)
- **Terminal**: [Kitty](https://sw.kovidgoyal.net/kitty)
- **Editor**: [Neovim](https://neovim.io)
- **File Manager**: [LF](https://pkg.go.dev/github.com/gokcehan/lf) - TODO: [config](https://github.com/gokcehan/lf/blob/master/etc/lfrc.example)
- **Mail**: TODO: Add config for [Neomutt](https://neomutt.org)
- **Browser**: [Browsh](https://www.brow.sh) & [Lynx](http://lynx.browser.org)
- **Theme**: [Nord](https://www.nordtheme.com)

Honorary mentions

**Audio**
- mpd
- ncmpcpp
- cava

- WIP


## Getting started

Just clone the repo to `~/.dotfiles` and fire it up:

```shell
# Clone the repository
git clone --recursive https://github.com/S1SYPHOS/dotfiles.git ~/.dotfiles

# Symlink all the things
cd ~/.dotfiles
bash install.sh

# Making an alias makes it even easier
alias dotbot="bash ~/.dotfiles/install.sh"
```


## Roadmap:
- [x] Move to [dotbot](https://git.io/dotbot)
- [x] Move to ~~i3~~/sway


## Credits
I got a lot of inspiration from similar projects & incredibly useful resources all over the internet, just to name a few:
- [bash-it](https://github.com/Bash-it/bash-it) (aliases, functions)
- [sensible-bash](https://github.com/mrzool/bash-sensible) (well, sane Bash defaults)
- [Tinu Weber](https://github.com/ayekat/dotfiles), [Terencio Agozzino](https://github.com/rememberYou/dotfiles) and others caring about XDG compliance
- [Emmanuel Rouat](https://tldp.org/LDP/abs/html/sample-bashrc.html) (`.bashrc` resources)
- .. several dotfile repositories hosted on Github and elsewhere .. some even believe, they are [meant to be forked](https://zachholman.com/2010/08/dotfiles-are-meant-to-be-forked) - but [what if they're not](https://www.anishathalye.com/2014/08/03/managing-your-dotfiles)?
